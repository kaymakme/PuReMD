#!/usr/bin/env python
import  os

'''
Simple parser for training data
TODO: right now, it is strict about the spaces, it could be solved with
a tokenizer: tokenize the input then parse the tokens
'''
def clear_comments(input_str):
    '''
    clear the comments from a line (only # type of comments for now)
    example:
        input:   chexane  0.01   1              0.0    # Eucledian distance
        output:  chexane  0.01   1              0.0
    '''
    com_start_ind = input_str.find('#')
    return input_str[:com_start_ind]

def parse_trainset(trainset_file='trainset.in'):
    parsed_cells = initial_parse_trainset(trainset_file)
    return process_parsed_data(parsed_cells)
    

def initial_parse_trainset(trainset_file='trainset.in'):
    '''
    reads all the lines *between* the section indicators in
    trainset.in and stores them in a cells dictionary.

    this is so I can append new lines to existing sections, and later
    write them back out. Each line is stripped.
    '''

    cells = {}
    cells['CHARGE'] = []
    cells['GEOMETRY'] = []
    cells['FORCES'] = []
    cells['CELL PARAMETERS'] = []
    cells['ENERGY'] = []
    cells['HEATFO'] = []
    if not os.path.exists(trainset_file):
        return cells

    CHARGE_FLAG, GEO_FLAG, FORCE_FLAG, CELL_PARAM_FLAG, ENERGY_FLAG, \
        HEAT_FLAG  = False, False, False, False, False, False
        
    f = open(trainset_file,'r')
    for line in f:
        line = clear_comments(line).strip()
        if len(line) < 1:
            continue        
        
        if line.startswith('CHARGE'):
                CHARGE_FLAG = True
                continue
        elif line.startswith('ENDCHARGE'):
                CHARGE_FLAG = False
                
        elif line.startswith('GEOMETRY'):
                GEO_FLAG = True
                continue
        elif line.startswith('ENDGEOMETRY'):
                GEO_FLAG = False  
                
        elif line.startswith('FORCES'):
                FORCE_FLAG = True
                continue
        elif line.startswith('ENDFORCES'):
                FORCE_FLAG = False
          
        elif line.startswith('CELL PARAMETERS'):
                CELL_PARAM_FLAG = True
                continue
        elif line.startswith('ENDCELL PARAMETERS'):
                CELL_PARAM_FLAG = False
                
        elif line.startswith('ENERGY'):
                ENERGY_FLAG = True
                continue
        elif line.startswith('ENDENERGY'):
                ENERGY_FLAG = False 
                

        elif line.startswith('HEATFO'):
                HEAT_FLAG = True
                continue
        elif line.startswith('ENDHEATFO'):
                HEAT_FLAG = False


            
        elif CHARGE_FLAG:
            cells['CHARGE'].append(line)
        elif GEO_FLAG:
            cells['GEOMETRY'].append(line)        
        elif FORCE_FLAG:
            cells['FORCES'].append(line)
        elif CELL_PARAM_FLAG:
            cells['CELL PARAMETERS'].append(line)
        elif ENERGY_FLAG:
            cells['ENERGY'].append(line)
        elif HEAT_FLAG:
            cells['HEATFO'].append(line)
        else:
            print('The input file is not in the correct format!')
    f.close()
    return cells


def process_parsed_data(cells):
    '''
     process the parsed data and create the required lists and dictionaries
     corresponding to the parsed data and print the required error messages
    '''
    processed_data = {}
    processed_data['CHARGE'] = []
    processed_data['GEOMETRY'] = []
    processed_data['FORCES'] = []
    processed_data['CELL PARAMETERS'] = []
    processed_data['ENERGY'] = []
    processed_data['HEATFO'] = []
    # process lines for charge
    for line in cells['CHARGE']: 
        # format: Key Acc Atom Ref
        words = line.split()
        if len(words) != 4:
            print('something wrong with charge data')
        charge_input = {}
        charge_input['key'] = words[0]
        charge_input['acc'] = words[1]
        charge_input['atom'] = words[2]
        charge_input['ref'] = words[3]
        processed_data['CHARGE'].append(charge_input)
        
    # process lines for geo
    for line in cells['GEOMETRY']: 
        # format: Key Acc [Atom1 [Atom2 [Atom3 [Atom4]]] Ref]
        words = line.split()
        if len(words) < 3 or len(words) > 7:
            print('something wrong with geometry data')
        geo_input = {}
        geo_input['key'] = words[0]
        geo_input['acc'] = words[1]
        if len(words) > 3: # can be multiple atoms or not at all
            geo_input['atoms'] = words[2:-1] 
        geo_input['ref'] = words[-1]
        processed_data['GEOMETRY'].append(geo_input)

    # process lines for forces
    for line in cells['FORCES']: 
        # format: Key Acc Type Ref
        words = line.split()
        if len(words) != 4:
            print('something wrong with force data')
        force_input = {}
        force_input['key'] = words[0]
        force_input['acc'] = words[1]
        force_input['atom'] = words[2]
        force_input['ref'] = words[3]
        processed_data['FORCES'].append(force_input)
        
    # process lines for CELL PARAMETERS
    for line in cells['CELL PARAMETERS']: 
        # format: Key Acc Type Ref
        words = line.split()
        if len(words) != 4:
            print('something wrong with CELL PARAMETERS data')
        force_input = {}
        force_input['key'] = words[0]
        force_input['acc'] = words[1]
        force_input['type'] = words[2]
        force_input['ref'] = words[3]
        processed_data['CELL PARAMETERS'].append(force_input)
        
    # process lines for ENERGY
    for line in cells['ENERGY']: 
        # format: Acc [+-] Key1/n1 ... [+-] Key5/n5 Ref
        words = line.split()
        '''
        if len(words) > 2 and len(words) % 2 == 0:
            print('something wrong with ENERGY data')
        '''
        energy_input = {}
        energy_input['acc'] = words[0]
        energy_input['parts'] = []
        item = {}
        for word in words[1:-1]:
            if word in ['+','-']:
                item = {}
                item['op'] = word
            elif word.find('/') > 0:
                word_sub_parts = word.split('/')
                item['key'] = word_sub_parts[0]
                item['n']  = word_sub_parts[1]
                energy_input['parts'].append(item)
            # if there is a space like in "Key1 /n1"
            else:
                if word.find('/') == 0:
                    item['n'] = word[1:]
                    energy_input['parts'].append(item)
                else:
                    item['key'] = word
                    
                
                
        energy_input['ref'] = words[-1]
        processed_data['ENERGY'].append(energy_input)
        
    # process lines for HEAT
    for line in cells['HEATFO']: 
        # format: Key Acc Type Ref
        words = line.split()
        if len(words) != 3:
            print('something wrong with HEAT data')
        heat_input = {}
        heat_input['key'] = words[0]
        heat_input['acc'] = words[1]
        heat_input['ref'] = words[2]
        processed_data['HEATFO'].append(heat_input)
        
    return processed_data
        
