#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 18 16:29:59 2019

@author: cagri
"""
import  os

def parse_geo_file(geo_file):
    if not os.path.exists(geo_file):
        return
    list_subruns = []
    f = open(geo_file,'r')
    run_str = ''
    for line in f:
        if len(line.strip()) < 1:
            list_subruns.append(run_str)
            run_str = ''
        else:
            run_str = run_str + line
    return list_subruns
            
